package edu.msu.anr.osgi.structuralintegrity.service;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.dotcms.repackage.org.apache.hadoop.mapred.lib.Arrays;
import com.dotmarketing.business.APILocator;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.util.Logger;

/**
 * Service providing Structures snapshot taking and management.
 * @author slenkeri
 */
public class StructuresSnapshotServiceImpl implements StructuresSnapshotService {

    /** The name of the dotCMS subdirectory in which Structural Integrity snapshot files will be saved. */
	private static final String SNAPSHOTS_DIR_NAME = "structuralIntegritySnapshots";

	StructuresSnapshotServiceImpl() throws IOException {
        // Ensure snapshots directory exists
        if (!this.getSnapshotsDir().exists()) {
            boolean succ = getSnapshotsDir().mkdir();
            if (!succ) {
                throw new IOException("Unable to create missing snapshots directory " + getSnapshotsDir().toString());
            }
        }
    }

    public StructuresSnapshot takeSnapshot() throws DotDataException, DotSecurityException {
        return new StructuresSnapshot();
    }
    
    public void saveSnapshot(StructuresSnapshot snapshot) throws IOException {
		// Create snapshot filepath
    	String fileName = snapshot.timestamp.format(DateTimeFormatter.ofPattern("yyyy-MM-dd_kk.mm.ss.SSS"));
		String snapshotFilePath = this.getSnapshotsDir().getAbsolutePath() + File.separator + fileName;

		// Write serialized snapshot to file
    	try {
			FileOutputStream fileOut = new FileOutputStream(snapshotFilePath);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(snapshot);
			out.close();
			fileOut.close();
    	} catch (FileNotFoundException e) {
    		Logger.error(this, "Unable to create snapshot file " + snapshotFilePath + ".");
    		throw e;
    	} catch (IOException e) {
    		Logger.error(this, "Failed to serialize snapshot to file " + snapshotFilePath + ".");
    		throw e;
    	}
    }
    
    public StructuresSnapshot loadSnapshotFromFile(File snapshotFile) throws IOException, ClassNotFoundException {
    	StructuresSnapshot loadedSnapshot;
    	try {
    		FileInputStream fileIn = new FileInputStream(snapshotFile);
    		ObjectInputStream in = new ObjectInputStream(fileIn);
    		loadedSnapshot = (StructuresSnapshot) in.readObject();
    		in.close();
    		fileIn.close();
    	} catch (IOException e) {
    		Logger.error(this, "Unable to read snapshot from file " + snapshotFile.getAbsolutePath() + ".");
    		throw e;
    	}
    	
    	return loadedSnapshot;
    }
    
    public StructuresSnapshot loadSnapshotFromFile(String snapshotFilePath) throws IOException, ClassNotFoundException {
		if (!snapshotFilePath.contains(File.separator)) {
            snapshotFilePath = getSnapshotsDir() + File.separator + snapshotFilePath;
        }

    	return this.loadSnapshotFromFile(new File(snapshotFilePath));
    }
    
    public File[] getSavedSnapshotFiles() {
    	return this.getSnapshotsDir().listFiles();
    }
    
    public File[] getSavedSnapshotFiles(FileFilter filter) {
    	return this.getSnapshotsDir().listFiles(filter);
    }
    
    public List<StructuresSnapshot> getSavedSnapshots() throws IOException, ClassNotFoundException {
    	List<StructuresSnapshot> loadedSnapshots = new ArrayList<>();
    	for (File snapshotFile : this.getSavedSnapshotFiles()) {
    		loadedSnapshots.add(this.loadSnapshotFromFile(snapshotFile));
    	}
    	return loadedSnapshots;
    }

    public StructuresSnapshot getPreviousSnapshot() throws IOException, ClassNotFoundException, IndexOutOfBoundsException {
    	// Get snapshot files
    	File[] allSnapshotFiles = this.getSavedSnapshotFiles();
    	if (allSnapshotFiles.length < 1) {
    		throw new IndexOutOfBoundsException("No previous snapshot exists.");
    	}

    	// Get most recent snapshot file
    	Arrays.sort(allSnapshotFiles);
    	File mostRecentSnapshotFile = (File) Array.get(allSnapshotFiles, allSnapshotFiles.length - 1);
    	
    	// Load and return most recent snapshot
    	return this.loadSnapshotFromFile(mostRecentSnapshotFile);
    }

    
    /**
     * Gets the directory where the snapshots are stored.
     * @return File object representing the snapshots directory.
     */
    private File getSnapshotsDir() {
    	String assetsRootPath = APILocator.getFileAssetAPI().getRealAssetsRootPath();
    	String snapshotsDirPath = assetsRootPath + File.separator + StructuresSnapshotServiceImpl.SNAPSHOTS_DIR_NAME;
    	return new File(snapshotsDirPath);
    }

}