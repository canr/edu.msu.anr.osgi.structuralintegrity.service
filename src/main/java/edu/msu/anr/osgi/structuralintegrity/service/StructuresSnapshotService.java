package edu.msu.anr.osgi.structuralintegrity.service;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;

import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;

/**
 * An OSGi service which provides functions used to take and manage snapshots of dotCMS' Structures.
 * @author slenkeri
 */
public interface StructuresSnapshotService {

    /**
     * Takes a timestamped snapshot of all structures in the system.
     * @return A timestamped snapshot of all structures in the system.
     * @throws DotDataException If a Contentlet cannot be reconstructed from the database.
     * @throws DotSecurityException If the system user cannot read content for some reason.
     */
    StructuresSnapshot takeSnapshot() throws DotDataException, DotSecurityException;

    /**
     * Saves a snapshot of all structures in the system.
     * @param snapshot A snapshot of all structures in the system.
     * @throws IOException If the snapshot cannot be serialized to disk.
     * @throws SecurityException If the snapshot file cannot be created.
     */
    void saveSnapshot(StructuresSnapshot snapshot) throws IOException;

    /**
     * Loads a saved snapshot from a snapshot file.
     * @param snapshotFile File containing a saved snapshot.
     * @return The snapshot saved in the given file.
     * @throws IOException If the snapshot could not be loaded and deserialized from the given file.
     * @throws ClassNotFoundException If the StructuresSnapshot class could not be loaded;
     */
    StructuresSnapshot loadSnapshotFromFile(File snapshotFile) throws IOException, ClassNotFoundException;

    /**
     * Loads a saved snapshot from a snapshot file.
     * @param snapshotFilePath Path to a file containing a saved snapshot. May be an absolute path or the name of a
     *                         snapshot file in the snapshots directory.
     * @return The snapshot saved in the given file.
     * @throws IOException if the snapshot could not be loaded and deserialized from the given file.
     * @throws ClassNotFoundException if the StructuresSnapshot class could not be loaded;
     */
    StructuresSnapshot loadSnapshotFromFile(String snapshotFilePath) throws IOException, ClassNotFoundException;

    /**
     * Gets all saved snapshot files.
     * @return Array containing all files in the snapshots directory.
     */
    File[] getSavedSnapshotFiles();

    /**
     * Gets all saved snapshot files which meet the given filter criteria.
     * @param filter Filter object.
     * @return Array containing all files in the snapshots directory which meet the given filter criteria.
     */
    File[] getSavedSnapshotFiles(FileFilter filter);

    /**
     * Gets all historic structures snapshots.
     * @return All saved snapshot files.
     * @throws IOException If the snapshot could not be loaded and deserialized from the given file.
     * @throws ClassNotFoundException If the StructuresSnapshot class could not be loaded;
     */
    List<StructuresSnapshot> getSavedSnapshots() throws IOException, ClassNotFoundException;

    /**
     * Gets the most recent saved structures snapshot.
     * @return The most recent saved structures snapshot.
     * @throws IOException if there is an error reading the snapshot from disk.
     * @throws ClassNotFoundException if the StructuresSnapshot class is not found.
     * @throws IndexOutOfBoundsException if there are no snapshots on disk.
     */
    StructuresSnapshot getPreviousSnapshot() throws IOException, ClassNotFoundException, IndexOutOfBoundsException;
}
