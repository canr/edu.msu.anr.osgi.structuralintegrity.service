package edu.msu.anr.osgi.structuralintegrity.service;

import com.dotcms.repackage.org.osgi.framework.BundleContext;
import com.dotmarketing.osgi.GenericBundleActivator;
import com.dotmarketing.util.Logger;

import java.util.Hashtable;


/**
 * OSGi Activator class provides methods for starting and stopping this bundle.
 * @author slenkeri
 */
public class Activator extends GenericBundleActivator {

    /**
     * Implements BundleActivator.start(). Registers an
     * instance of a test service using the bundle context;
     * attaches properties to the service that can be queried
     * when performing a service look-up.
     *
     * @param bundleContext the framework context for the bundle.
     */
    @Override
    public void start ( BundleContext bundleContext ) throws Exception {
        // Set service properties
        Hashtable<String, String> props = new Hashtable<>();

        // Register service
        bundleContext.registerService( StructuresSnapshotService.class.getName(), new StructuresSnapshotServiceImpl(), props );
        Logger.info(this, "Registered service " + StructuresSnapshotService.class.getName());

        // Publish services
        publishBundleServices(bundleContext);
    }

    /**
     * Implements BundleActivator.stop(). Does nothing since
     * the framework will automatically unregister any registered services.
     *
     * @param bundleContext the framework context for the bundle.
     */
    @Override
    public void stop ( BundleContext bundleContext ) throws Exception {
        // NOTE: The service is automatically unregistered.
    }

}