package edu.msu.anr.osgi.structuralintegrity.service;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.portlets.structure.factories.StructureFactory;
import com.dotmarketing.portlets.structure.model.Structure;


/**
 * An instantaneous snapshot of all Structures in the CMS.
 * @author slenkeri
 */
public class StructuresSnapshot implements Serializable {

	/** List of all structure snapshots in the system. */
	public List<StructureSnapshot> structureSnapshots = new ArrayList<>();
	
	/** Snapshot creation time. */
    public final LocalDateTime timestamp = LocalDateTime.now();

	private static final long serialVersionUID = 1L;

    /**
     * Takes a timestamped snapshot of all Structures in the CMS.
     * @throws DotDataException If a Contentlet cannot be reconstructed from the database.
     * @throws DotSecurityException If the system user cannot read content for some reason.
     */
    public StructuresSnapshot() throws DotDataException, DotSecurityException {
    	List<Structure> structures = StructureFactory.getStructures();
    	for (Structure structure : structures) {
			this.structureSnapshots.add(new StructureSnapshot(structure));
    	}
    }
    
    /**
     * Determines whether two lists of StructureSnapshots are equal.
     * @param other The StructuresSnapshot to which this StructuresSnapshot should be compared.
     * @return Whether the two StructuresSnapshots' lists of StructureSnapshots are equal.
     */
    public boolean isEquivalent(StructuresSnapshot other) {
    	// New structures?
    	if (this.structureSnapshots.size() != other.structureSnapshots.size()) {
    		return false;
    	}
    	
    	// Have existing structures changed?
    	for (int i = 0; i < this.structureSnapshots.size(); i++) {
    		if (!this.structureSnapshots.get(i).isEquivalent(other.structureSnapshots.get(i))) {
    			return false;
    		}
    	}
    	
    	return true;
    }

}