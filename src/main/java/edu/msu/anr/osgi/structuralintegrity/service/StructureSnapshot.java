package edu.msu.anr.osgi.structuralintegrity.service;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

import com.dotmarketing.business.APILocator;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.portlets.contentlet.model.Contentlet;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.util.Logger;

/**
 * An instantaneous snapshot of a Structure's definition and sample Contentlets.
 * @author slenkeri
 */
public class StructureSnapshot implements Serializable {

	/** The structure whose state is to be saved. */
	public final Structure structure;

	/** The 10 oldest instances of this structure in the CMS. */
    public List<Contentlet> samples = new ArrayList<>();
    
    private static final int NUM_SAMPLES = 10;
	private static final long serialVersionUID = 1L;


    /**
     * Constructs a snapshot of a given Structure.
     * @param structure The structure whose definition and samples are to be recorded.
     * @throws DotDataException If a Contentlet cannot be reconstructed from the database.
     * @throws DotSecurityException If the system user cannot read content for some reason.
     */
    public StructureSnapshot(Structure structure) throws DotDataException, DotSecurityException {
    	this.structure = structure;
		try {
			this.samples = APILocator.getContentletAPI().search(
					"+contentType:\"" + structure.getVelocityVarName() + "\"",	// Query
					StructureSnapshot.NUM_SAMPLES,  							// Limit
					0,															// Offset
					"modDate asc",												// Sort
					APILocator.getUserAPI().getSystemUser(),					// User
					false);														// Respect frontend roles
		} catch (DotDataException e) {
			Logger.error(this, "Unable to read contentlet.");
			throw e;
		} catch (DotSecurityException e) {
			Logger.error(this, "System user can't access contentlet.");
			throw e;
		}
    }
    
    /**
     * Determines whether two StructureSnapshots are equal.
     * @param other The StructureSnapshot to which this StructureSnapshot should be compared.
     * @return Whether the two StructureSnapshots' lists of StructureSnapshots are equal.
     */
    public boolean isEquivalent(StructureSnapshot other) {
    	// Structures equal?
    	if (!this.structure.getMap().equals(other.structure.getMap())) {
    		return false;
    	}
    	
    	// New samples?
    	if (this.samples.size() != other.samples.size()) {
    		return false;
    	}
    	
    	// Samples same?
    	for (int i = 0; i < this.samples.size(); i++) {
    		if (!this.samples.get(i).equals(other.samples.get(i))) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
}